/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"
#include "string.h"
#include "stdbool.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/**
 * @brief enum reflects names of led blinking phases.
 */
typedef enum {
	RISE = 0,
	PLATO,
	RELEASE,
	LED_PHASES_NUM,
} led_blink_phases;

/**
 * @brief	struct to describe a way how led is blinking.
 */
typedef struct led_blink_mode {
	uint32_t dur[LED_PHASES_NUM]; /**< time periods for each phase in
									   msecs */
	uint8_t	max_brightness; 	  /**< percentage of maximum brightness */
	uint32_t elapsed;             /**< time in msec which elapsed since
	 	 	 	 	 	 	 	 	   start of the system */
	bool settled;				  /**< reflect is a brightness for current
									   time was settled */
} led_blink_mode;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define TIM2_PERIOD		(1049)
#define TIM2_PRESCALER	(16)

#define TIM4_PERIOD 	(1000)
#define TIM4_PRESCALER	(16)

#define LED_MAX_PULSE_PERIODS  TIM4_PERIOD
/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void LED_Modes_init(void);
void ChangeTIM4Pulse(uint32_t pulse);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
