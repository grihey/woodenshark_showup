## Test task for Woodenshark.

Implementation of different led blinking modes on stm32l1 discovery board 
with stm32l152rct6 microcontroller.

Task implemented in STM32CudeIDE which could be downloaded from
https://www.st.com/en/development-tools/stm32cubeide.html#get-software

Terms:
 * period (ms) - duration of whole blinking cycle
 * phase (ms) - duration of particular cycle phase (RISE, PLATO, RELEASE)